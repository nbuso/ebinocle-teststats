export class ReportRow {

  Label: string;
  Samples: number;
  Average: number;
  Median: number;
  Line_90: number;
  Line_95: number;
  Line_99: number;
  Min: number;
  Max: number;
  ErrorPercentage: number;
  Throughput: number;
  ReceivedKBsec: number;
  SentKBsec: number;
}
