import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Injectable({
  providedIn: 'root'
})
export class HeaderTitleService {
  public subtitle: string;

  constructor(private titleSrv: Title) { }

  get title() {
    return this.titleSrv.getTitle();
  }

  set title(title: string) {
    this.titleSrv.setTitle(title);
  }


}
