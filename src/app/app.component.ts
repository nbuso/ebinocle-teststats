import { Component, OnInit } from '@angular/core';

import { HeaderTitleService } from './header-title/header-title.service';

@Component({
  selector: 'estests-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private headerTitleSrv: HeaderTitleService) {
    this.headerTitleSrv.title = "Home"
    this.headerTitleSrv.subtitle = "JMeter results visualization"
  }

  public ngOnInit() {
  }
}
