import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BarComparisonPageComponent } from './pages/bar-comparison-page/bar-comparison-page.component';
import { SummaryComponent } from './pages/summary/summary.component';

const routes: Routes = [
  { path: '', redirectTo: 'summary', pathMatch: 'full' },
  { path: 'summary', component: SummaryComponent },
  { path: 'bar-comparison', component: BarComparisonPageComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
