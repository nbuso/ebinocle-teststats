import { ReportRow } from './report-row';

export class Report {
  name: string;
  header: string[];
  rows: ReportRow[];
  footer: ReportRow;
}
