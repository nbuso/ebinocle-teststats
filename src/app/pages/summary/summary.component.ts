import { Component, OnInit } from '@angular/core';

import { HeaderTitleService } from '../../header-title/header-title.service';

@Component({
  selector: 'estests-summary',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.scss']
})
export class SummaryComponent implements OnInit {

  constructor(
    private headerTitleSrv: HeaderTitleService) {

    this.headerTitleSrv.title = "Summary"
  }

  ngOnInit(): void {
  }

}
