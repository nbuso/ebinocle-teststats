import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts';

import { MatSelectModule } from '@angular/material/select';
import { MatFormFieldModule } from '@angular/material/form-field';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComparisonBarComponent } from './graphs/comparison-bar/comparison-bar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { BarComparisonPageComponent } from './pages/bar-comparison-page/bar-comparison-page.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { SummaryComponent } from './pages/summary/summary.component';

@NgModule({
  declarations: [
    AppComponent,
    ComparisonBarComponent,
    BarComparisonPageComponent,
    HeaderComponent,
    FooterComponent,
    SummaryComponent,
  ],
  imports: [
    BrowserModule,
    ChartsModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,

    MatFormFieldModule,
    MatSelectModule,
    BrowserAnimationsModule,

    AppRoutingModule,

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
