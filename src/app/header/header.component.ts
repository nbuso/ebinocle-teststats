import { Component, OnInit } from '@angular/core';

import { HeaderTitleService } from '../header-title/header-title.service';

@Component({
  selector: 'estests-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor(private headerTitleSrv: HeaderTitleService) {
  }

  ngOnInit(): void {
  }

  get title() {
    return this.headerTitleSrv.title;
  }

  get subtitle() {
    return this.headerTitleSrv.subtitle;
  }
}
