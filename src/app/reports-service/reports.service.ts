import { Injectable } from '@angular/core';
import { combineLatest, of, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

import { Report } from './entities/report';
import { ReportRow } from './entities/report-row';

@Injectable({
  providedIn: 'root'
})
export class ReportsService {
  private files = [
    'isilon_vm_aggregate.csv',
    'isilon_vm_aggregate_k8s.csv',
    'isilon_aggregate_k8s_dev.csv',
    'isilon_aggregate_k8s_prod.csv',
    'pure_vm_aggregate.csv',
    'pure_ip_mounted_vm_aggregate.csv',
  ];


  // 'isilon_k8s_aggregate_12GB.csv',
  // 'isilon_k8s_aggregate_24GB.csv',
  // 'isilon_aggregate.csv',
  // 'pure_aggregate.csv',

  private reportsEndpoint = 'assets/reports';

  constructor(private httpClient: HttpClient) {}

  public listFiles(): string[] {
    return this.files;
  }

  public readAllReports(): Observable<Report[]> {
    const observables: Observable<Report>[] = [];
    this.files.forEach(f => observables.push(this.readReportFromFile(f)));
    return combineLatest(observables);
  }

  public readReportFromFile(fileName: string): Observable<Report> {
    if (this.files.filter(f => f === fileName)) {
      return this.httpClient.get(`${this.reportsEndpoint}/${fileName}`, { responseType: 'text' })
        .pipe(
          map(content => {
            const report = {
              name: fileName,
              header: undefined,
              rows: undefined,
              footer: undefined,
            };
            let csvRecordsArray = (<string>content).split(/\r\n|\n/);

            report.header = this.getHeaderArray(csvRecordsArray);
            const records = this.getReportRowsFromCSVFile(csvRecordsArray, report.header.length, csvRecordsArray.length - 1);
            report.rows = records.slice(0, records.length - 1).sort((a, b) => a.Label.localeCompare(b.Label));
            report.footer = records[records.length - 1];
            return report;
          })
        );
    } else {
      const error = `No file present with name: ${fileName}`;
      console.error(error);
      return undefined;
    }
  }

  getReportRowsFromCSVFile(csvRecordsArray: string[], headerLength: number, totalRows: number): ReportRow[] {
    let csvArr: ReportRow[] = [];

    for (let i = 1; i < totalRows; i++) {
      let currentRecord = (<string>csvRecordsArray[i]).split(',');
      if (currentRecord.length == headerLength) {
        const reportRow = this.mapReportRow(currentRecord);
        csvArr.push(reportRow);
      }
    }
    return csvArr;
  }

  mapReportRow(valuesArray: string[]): ReportRow {
    const reportRow: ReportRow = new ReportRow();
    reportRow.Label = valuesArray[0].trim();
    reportRow.Samples = parseInt(valuesArray[1].trim());
    reportRow.Average = parseInt(valuesArray[2].trim());
    reportRow.Median = parseInt(valuesArray[3].trim());
    reportRow.Line_90 = parseInt(valuesArray[4].trim());
    reportRow.Line_95 = parseInt(valuesArray[5].trim());
    reportRow.Line_99 = parseInt(valuesArray[6].trim());
    reportRow.Min = parseInt(valuesArray[7].trim());
    reportRow.Max = parseInt(valuesArray[8].trim());
    reportRow.ErrorPercentage = parseInt(valuesArray[9].trim());
    reportRow.Throughput = parseInt(valuesArray[10].trim());
    reportRow.ReceivedKBsec = parseInt(valuesArray[11].trim());
    reportRow.SentKBsec = parseInt(valuesArray[12].trim());

    return reportRow;
  }

  getHeaderArray(csvRecordsArr: any) {
    let headers = (<string>csvRecordsArr[0]).split(',');
    let headerArray = [];
    for (let j = 0; j < headers.length; j++) {
      headerArray.push(headers[j]);
    }
    return headerArray;
  }
}
