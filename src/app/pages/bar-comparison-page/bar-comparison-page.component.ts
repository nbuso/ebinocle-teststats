import { Component, Input, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { ChartDataSets, ChartOptions, ChartType } from 'chart.js';

import { HeaderTitleService } from '../../header-title/header-title.service';
import { Report } from '../../reports-service/entities/report';
import { ReportsService } from '../../reports-service/reports.service';

@Component({
  selector: 'estests-bar-comparison-page',
  templateUrl: './bar-comparison-page.component.html',
  styleUrls: ['./bar-comparison-page.component.scss']
})
export class BarComparisonPageComponent implements OnInit {
  reports: Report[];

  public dataSets: any = {};
  public labelsSets: any = {};

  public availableMeasures: string[];
  public measuresControl = new FormControl('Average');

  public Object = Object;

  public testLabelRegexpFilters = [
    /[.]*/g,
  ];

  constructor(private reportSrv: ReportsService,
    private headerTitleSrv: HeaderTitleService) {

    this.headerTitleSrv.title = "Bar Comparison"
  }

  public ngOnInit() {
    this.reportSrv.readAllReports().subscribe(reports => {
      this.reports = reports;
      if (!this.availableMeasures) { this.availableMeasures = reports[0].header
        .filter(h => h !== 'Label' && h !== '# Samples' ); };
      this.loadDataSets();
    });
  }

  private loadDataSets() {
    if (this.reports) {
      this.dataSets = {};
    }
    this.testLabelRegexpFilters.forEach(regexp => this.setChartDataByPrefixLabel(regexp));
  }

  public getTestTitle(label: string): string {
    return `${label.replace(/^\w/, c => c.toUpperCase())} tests`
  }

  public getTestLabels(prefix): string[] {
    return this.labelsSets[prefix];
  }

  private setChartDataByPrefixLabel(regexp: RegExp): ChartDataSets[] {
    const records: ChartDataSets[] = [];
    let toReturn = this.dataSets[regexp.source];
    if (toReturn) { return toReturn; }
    if (this.reports) {
      const labels = [];
      this.reports.forEach(rep => {
        const dataSet = {
          label: rep.name,
          data: [],
        };
        rep.rows.filter(r1 => {
            return regexp.test(r1.Label);
        }).forEach(r2 => {
          dataSet.data.push(r2[this.measuresControl.value]);
          if (labels.indexOf(r2.Label) < 0) {
            labels.push(r2.Label);
          }
        });
        records.push(dataSet);
      });
      this.dataSets[regexp.source] = records;
      this.labelsSets[regexp.source] = labels;
    }
  }

  public onMeasureChange(): void {
    this.loadDataSets();
  }

}
