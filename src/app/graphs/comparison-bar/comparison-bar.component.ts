import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import {
  ChartDataSets,
  ChartOptions,
  ChartType,
} from 'chart.js';
import { BaseChartDirective } from 'ng2-charts';

@Component({
  selector: 'estests-comparison-bar',
  templateUrl: './comparison-bar.component.html',
  styleUrls: ['./comparison-bar.component.scss']
})
export class ComparisonBarComponent implements OnInit {
  @ViewChild(BaseChartDirective)
  public chart: BaseChartDirective;

  @Input()
  title: string;

  @Input()
  dataSets: ChartDataSets[];

  @Input()
  labels: string[];

  chartOptions: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
        type: 'linear',
        scaleLabel: {
          display: true,
          labelString: 'Milliseconds'
        }
      }]
    },
  };

  chartLegend: true;
  chartPlugins = [];

  yAxesScaleTypeControl = new FormControl('linear', Validators.required);
  scaleTypes = [
    'linear',
    'logarithmic'
  ];

  constructor() { }

  ngOnInit(): void {
  }

  public onScaleChange() {
    this.chartOptions.scales.yAxes[0].type = this.yAxesScaleTypeControl.value;
    this.chart.ngOnChanges({}); 
  }
}
